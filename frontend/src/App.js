import logo from "./logo.svg";
import "./App.css";
import axios from "axios";
import { useEffect, useState } from "react";
import Login from "./Components/Login";
import Signin from "./Components/Signin";
import Home from "./Components/Home"
import Player from "./Components/Player.js"
import { BrowserRouter as Router, Switch, Link, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/login" component={Login}>
          </Route>
          <Route path="/signin" component={Signin}>
          </Route>
	    <Route path="/home" component={Home}>
	  </Route>
	    <Route path="/player" >
		<Player />
	    </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
