import React from "react";
import ReactPlayer from "react-player"
import {withRouter} from "react-router"
import "./YoutubePlayer.css"

class YoutubePlayer extends React.Component{
    constructor(props){
	super(props);
    }

    render(){

    let _url = "https://www.youtube.com/watch?v=" + (this.props.videoId)
	console.log(_url)
	return (
	    <ReactPlayer className="youtube-player"
		url = {_url}
		width = "800px"
		height= "500px"
	    />
	)
    }
}

export default withRouter(YoutubePlayer);
