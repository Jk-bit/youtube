//import logo from "./logo.svg";
//import "./App.css";
import "./Home.css"
import Header from "./Header.js";
import Sidebar from "./Sidebar.js";
import Videos from "./Videos.js";
import VideoDisplay from "./VideoDisplay.js"
import Player from "./Player.js"
import React, { useState } from "react";
import {withRouter, BrowserRouter as Router, Route, Switch} from "react-router-dom";
import axios from "axios"
import {useEffect} from "react"
import {useHistory} from "react-router-dom"
import Subscriptions from "./Subscriptions.js"
import Trending from "./Trending.js"
import {ListGroup} from "react-bootstrap"




function Home(props) {
  const [open, setOpen] = useState(false);
    const [videoList, setVideoList] = useState([]);
    const [showHome, setShowHome] = useState(true);
    const [showSubscribe, setShowSubscribe] = useState(false);
    const [showTrending, setShowTrending] = useState(false)
    const [trendingList, setTrendingList] = useState([])
    const history = useHistory();
    let mainDisplay
    useEffect(() => {    
	console.log(props.location.state.emailaddress)
    axios.get("http://localhost:5000/searchList", {params : {emailaddress : props.location.state.emailaddress}})
	.then((resp) => {
	    console.log(resp.data.items)
	    if(resp.data.items != []){
		setVideoList(resp.data.items)	    
	    }
	})
	.catch((err) => {
	    console.log(err)
	})
	/*axios.get("http://localhost:5000/trending")
	    .then((resp) => {
		setTrendingList(resp.data.items)

	    })
	    .catch((err) => {
		console.log(err)
	    })*/

    }, [])
    if(showHome){
	mainDisplay = <VideoDisplay videoList={videoList} />
    }
    else if(showSubscribe){

	mainDisplay = (
			<ListGroup>
			    <Subscriptions emailaddress={props.location.state.emailaddress}/>
			</ListGroup>	
		)
    }
    else if(!showTrending){
	mainDisplay =(<Trending videoList={trendingList}/>)
    }
	

  return (
    <div className="App">
	<Header open={open} setOpen={setOpen} videoList={videoList} setVideoList={setVideoList} emailAddress={props.location.state.emailaddress} history={history} />
      <div className="main">
	<div className="sidebar">
	    <Sidebar open={open}  emailaddress={props.emailaddress} showSubscribe={showSubscribe} setShowSubscribe={setShowSubscribe} showHome={showHome} showTrending={showTrending} setShowTrending={setShowTrending} setShowHome={setShowHome}/>
	</div>
	<div className="videos">
	    {mainDisplay}

	</div>
      </div>
    </div>
  );
}

export default withRouter(Home);
