import React from "react";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import VideocamIcon from "@material-ui/icons/Videocam";
import AppsIcon from "@material-ui/icons/Apps";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Avatar from "@material-ui/core/Avatar";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Header.css";
import { InputGroup } from "react-bootstrap";
import { FormControl } from "react-bootstrap";
import { Image } from "react-bootstrap";
import {Link} from "react-router-dom"
import axios from "axios";
import {BrowserRouter, Route, Redirect} from "react-router-dom"
class Header extends React.Component {
    constructor(props){
	super(props);
	this.state = {
	    search : ""
	}
    }

    deleteAccount = () =>{
	console.log(this.props.history)
	axios.get("http://localhost:5000/email", {params : {email : this.props.emailAddress}})
	    .then(resp => {
		this.props.history.push("/login")
	    })
	    .catch((err) => {
		console.log(err)
	    })
	
    }

  menuopen = () => {
    console.log("clicked");
    this.props.setOpen(!this.props.open);
  };

    search = () => {
	let response = {
	    item: "",
	}
	console.log(this.props.emailaddress)
	axios.get("http://localhost:5000/search", {params : {search : this.state.search, emailaddress : this.props.emailAddress}})
	    .then((resp) => {
		console.log((resp.data.items))
		response.item = resp.data.items
		this.props.setVideoList(resp.data.items)
	    })
	    .catch((err) => {
		console.log(err)
	    })
    }

  render() {
    return (
      <div className="header">
        <div className="header-logo">
          <Button href="#" onClick={this.menuopen} variant>
            <MenuIcon />
          </Button>

          <Image
            src="/home/jatish/Academics/WST/Project/Youtube_clone/youtube/src/Images/youtube_logo.png/"
            thumbnail
          />
        </div>
        <div className="header-input">
          <InputGroup className="mb-3">
	      <FormControl placeholder="Search" onChange={(e) => {this.setState({search : e.target.value})}}/>
            <InputGroup.Append>
              <Button href="#" variant onClick={this.search}>
                <SearchIcon />
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </div>
        <div className="header-icons">
          <Button href="#" variant>
            <VideocamIcon />
          </Button>{" "}
          <Button href="#" variant>
            <AppsIcon />
          </Button>{" "}
          <Button variant="primary" onClick={this.deleteAccount} className="button">
            DELETE ACCOUNT
          </Button>
		{" "}
	    <Link to="/login">
	    <Button variant="primary"  className="button">
            LOGOUT
          </Button>
	    </Link>
        </div>
      </div>
    );
  }
}
export default Header;
