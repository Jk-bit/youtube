import React from "react"
import axios from "axios"
import {ListGroup, Button} from "react-bootstrap"
import SubscriptionRow from "./SubscriptionRow.js"

class Subscriptions extends React.Component {
    constructor(props){
	super(props)
	this.state = {
	    loaded : false,
	    subscriptions : [],
	}
    }
    
    componentDidMount(){
	let subscriptions;
	axios.get("http://localhost:5000/subscriptions", {params : {emailaddress : this.props.emailaddress}}).
	    then((resp) => {
		console.log(resp.data.subscriptions)
		this.setState({subscriptions : resp.data.subscriptions})
	    })
	    .catch((err) => {
	    })
	this.setState({loaded : true})

    }

    
    render(){

	if(this.state.loaded){
	    return(
		this.state.subscriptions.map((elem, index) => {
		    return(
			<SubscriptionRow id={index} elem={elem} emailaddress={this.props.emailaddress} onClick={this.deleteSubscription}/>
		    )
		})
	    )
	}
	else{
	    return(
		<h1>Loading</h1>
	    )
	}
    }
}

export default Subscriptions
