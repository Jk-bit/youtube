import React from "react";
import "./Player.css"
import YoutubePlayer from "./YoutubePlayer.js"
import Header from "./Header.js"
import SidePlayer from "./SidePlayer.js"
import {withRouter} from "react-router"

class Player extends React.Component{
    constructor(props){
	super(props);
    }
    render(){
	console.log("Youtube : " + this.props.location.state.videoList)
	return(
	    <div className="main">
		<div className="youtube-player">
		    <YoutubePlayer videoId={this.props.location.state.videoId}/>
		</div>
		<div className="side-player">
		    <SidePlayer videoList={this.props.location.state.videoList}/>
		</div>
	    </div>
	)
    }
}

export default withRouter(Player);
