import React from "react"
import {ListGroup, Button} from "react-bootstrap"
import axios from "axios"
class SubscriptionRow extends React.Component {
    constructor(props){
	super(props)
    }
deleteSubscription = (event) => {
	axios.get("http://localhost:5000/deleteSubscription", {params : {subscriptions : this.props.elem, emailaddress : this.props.emailaddress}})
	    .then((resp) => {
		event.target.style.display = "none"
	    })
	    .catch((err) => {
		console.log(err)
	    })
	    
    }

    render(){
	return(
	    <ListGroup.Item className="subscription-row" onClick={this.deleteSubscription} action>{this.props.elem}
	    </ListGroup.Item>
	)
    }
}

export default SubscriptionRow;
