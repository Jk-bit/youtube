import React from "react"
import {withRouter} from "react-router"
import SideVideos from "./SideVideos.js"
import "./SideVideos.css"

class SidePlayer extends React.Component{
    constructor(props){
	super(props)
    }

    

    render(){
	let videoLists = this.props.videoList
	    return(	
		videoLists.map((elem, index) => {
		    return (
			<SideVideos thumbnail={elem[3][0]} title={elem[4]} desc={elem[2]} channelName={elem[1]} videoId={elem[0]} videoList={this.props.videoList}/>)		   })
		)
	}
}

export default withRouter(SidePlayer)
