import React from "react"
import "./SideVideos.css"
import {Link} from "react-router-dom"

class SideVideos extends React.Component {
    constructor(props){
	super(props);
    }
    render(){
	return (
	    <div className="sidevideo-box">
		<div classname="video-thumbnail" >   
			<Link to={{pathname : "/player",
			    state : {videoId : this.props.videoId , videoList : this.props.videoList} }}>
			    <img src={this.props.thumbnail} width="200px" height="100px"></img>
			</Link>
		    </div>
		    <div className="sidevideo-info">
			<h7>{this.props.title}</h7>
			<p>{this.props.channelName}</p>
		    </div>
	    </div>
	)
    }
}

export default SideVideos;
