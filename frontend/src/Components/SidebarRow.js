import React from "react";
import { Nav } from "react-bootstrap";

function SidebarRow(props) {
  return (
    <Nav.Link href="/home">
      {props.icon}
      {"   " + String(props.title)}
    </Nav.Link>
  );
}

export default SidebarRow;
