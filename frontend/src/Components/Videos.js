import React from "react";
import "./Videos.css";
import {Container, Col, Row, Image} from "react-bootstrap";
import {Link } from "react-router-dom";
class Videos extends React.Component {
    constructor(props){
	super(props);

    }
  render() {

    console.log(this.props.videoId)
      return (

		<div className="video-box">
		    <div classname="video-thumbnail" >   
			<Link to={{pathname : "/player",
			    state : {videoId : this.props.videoId , videoList : this.props.videoList} }}>
			<img src={this.props.thumbnail} width="300px" height="200px"></img>
			</Link>
		    </div>
		    <div className="video-info">
			<h4>{this.props.title}</h4>
			<p>{this.props.desc}</p>
			<h5>{this.props.channelName}</h5>
		 
		    </div>
		</div>
	    
      );
  }
}

export default Videos;
