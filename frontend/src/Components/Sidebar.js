import React from "react";
import { Nav } from "react-bootstrap";
import "./Sidebar.css";
import HomeIcon from "@material-ui/icons/Home";
import ExploreIcon from "@material-ui/icons/Explore";
import SubscriptionsIcon from "@material-ui/icons/Subscriptions";
import SettingsIcon from "@material-ui/icons/Settings";
import {Button} from "react-bootstrap"
import Subscriptions from "./Subscriptions.js"
import Trending from "./Trending.js"
class Sidebar extends React.Component {
    constructor(props){
	super(props);
    }

    home = () => {
	if(!this.props.showHome){
	    this.props.setShowHome(!this.props.showHome)
	    this.props.setShowSubscribe(false)
	    this.props.setShowTrending(false)
	}
    }

    subscribe = () => {
	if(!this.props.showSubscribe){
	    this.props.setShowSubscribe(!this.props.showSubscribe)
	    this.props.setShowHome(false)
	    this.props.setShowTrending(false)
	}
    }

    trending = () => {
	console.log(this.props.showTrending)
	    this.props.setShowHome(false)
	    this.props.setShowTrending(false)
    }

  render() {
    let sidebar_style;
    if (this.props.open) {
      sidebar_style = {
        flex: 0.3,
        transform: "translateX(0)",
      };
    } else {
      sidebar_style = {
        flex: 0,
        transform: "translateX(-100%)",
      };
    }
    return (
      <div className="sidebar" style={sidebar_style}>
        <Nav defaultActiveKey="/home" className="flex-column" variant="pills">
          <Nav.Item>
	      <Nav.Link onClick={this.home} >
              <HomeIcon />
              {"                "}
              Home
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onClick={this.trending}>
              <ExploreIcon />
              Trending
            </Nav.Link>
          </Nav.Item>

          <Nav.Item>
	      <Nav.Link onClick={this.subscribe}>
              <SubscriptionsIcon />
              Delete Subscriptions
	      </Nav.Link>
	  </Nav.Item>

          <hr id="hr" />
        </Nav>
      </div>
    );
  }
}

export default Sidebar;
