import React from "react"
import Videos from "./Videos.js"
import axios from "axios"
class Trending extends React.Component {
    constructor(props){
	super(props);
	this.state = {
	    videoList :[],
	    loading : true,
	}
    }
    

    componentDidMount(){
	axios.get("http://localhost:5000/trending")
	    .then((resp) => {
		this.setState({videoList : resp.data.items})
	    })
	    .catch((err) => {
		console.log(err)
	    })
    }

    render(){
	
	let videoLists = this.props.videoList
	return(
	this.state.videoList.map((elem, index) => {
		return (
		    <Videos thumbnail={elem[3][0]} title={elem[4]} desc={elem[2]} channelName={elem[1]} videoId={elem[0]} videoList={this.state.videoList}/>)})
	)



    }
}
export default Trending;
