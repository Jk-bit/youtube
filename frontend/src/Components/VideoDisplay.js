import React from "react";
import Videos from "./Videos.js"
class VideoDisplay extends React.Component{
    constructor(props){
	super(props);
    }
    render(){
	let videoLists = this.props.videoList;
	


	return(
	    videoLists.map((elem, index) => {
		return (
		    <Videos thumbnail={elem[3][0]} title={elem[4]} desc={elem[2]} channelName={elem[1]} videoId={elem[0]} videoList={this.props.videoList}/>)})
	
	) }
}

export default VideoDisplay;
