import React from "react";
import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Login.css";
import axios from "axios";
import {withRouter} from "react-router";

class Signin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      emailAddr: "",
      password: "",
	email_present : "",
    };

  }

  onSubmitForm = (event) => {
      event.preventDefault()
    console.log(this.state.firstName);
    let request_data = {
      firstname: this.state.firstName,
      lastname: this.state.lastName,
      emailaddress: this.state.emailAddr,
      password: this.state.password,
    };
    axios
      .post("http://127.0.0.1:5000/signin", request_data)
      .then((res) => {
        let response_data = res.data;

        console.log(response_data);
	  if(res.data.error == true){
	      this.setState({email_present : "Email Address already registered"})
	  }
	  else{
	    this.props.history.push("/login");
	  }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <div>
	  <Form className="form" method="POST" onSubmit={this.onSubmitForm}>
          <Row>
            <Col>
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="First Name"
                required
                onChange={(e) => this.setState({ firstName: e.target.value })}
              ></Form.Control>
            </Col>
            <Col>
              <Form.Label className="form-label">Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Last Name"
                className="form-field"
                onChange={(e) => this.setState({ lastName: e.target.value })}
                required
              ></Form.Control>
            </Col>
          </Row>
          <Form.Group controlId="formGroupEmail">
            <Form.Label className="form-label">Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter your Email"
              className="form-field"
              onChange={(e) => this.setState({ emailAddr: e.target.value })}
              required
            ></Form.Control>
	      <p className="warning">{this.state.email_present}</p>
          </Form.Group>
          <Form.Group controlId="formGroupPassword">
            <Form.Label className="form-label">Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter your password"
              className="form-field"
              onChange={(e) => this.setState({ password: e.target.value })}
              required
            ></Form.Control>
	      <p>{this.state.email_present}</p>
          </Form.Group>
          <Button type="submit" variant="primary">
            SIGN IN
          </Button>
        </Form>
      </div>
    );
  }
}

export default withRouter(Signin);
