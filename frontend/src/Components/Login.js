import React from "react";
import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Login.css";
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
class Login extends React.Component {
    constructor(props){
	super(props);
	this.state = {
	    emailaddress : "",
	    password : "",
	    invalid_password : "",
	    no_username : ""
	}
    };

    onSubmitForm = (event) => {
	event.preventDefault();
	event.persist();
	let request_data = {
	    emailaddress : this.state.emailaddress,
	    password : this.state.password
	}
	axios.post("http://localhost:5000/login", request_data)
	    .then((response) => {
		if(response.data.invalid_password == true){
		    this.setState({invalid_password : "Invalid Password"});
		}
		else if(response.data.no_username == true){
		    this.setState({no_username : "Invalid Username"});
		}
		else{
		    this.props.history.push({
			pathname : "/home",
			state : {"emailaddress" : this.state.emailaddress, "subscription" : false}
		    });
		}
	    })
	    .catch(err => {console.log(err)});
    }
  render() {
    return (
      <div>
        <Form onSubmit={this.onSubmitForm} method="POST" className="form">
          <h3>YouTube</h3>

          <Form.Group controlId="formGroupEmail">
            <Form.Label float="left" className="form-label" required>
              Email address
            </Form.Label>
            <Form.Control
              className="form-field"
              type="email"
              placeholder="Enter your email"
              required
		onChange={(e) => this.setState({emailaddress : e.target.value})}
            ></Form.Control>
	      <p className="warning"> {this.state.no_username} </p>
          </Form.Group>

          <Form.Group controlId="formGroupPassword">
            <Form.Label className="form-label">Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              className="form-field"
              required
	      onChange = {(e) => this.setState({password : e.target.value})}
            ></Form.Control>
	      <p className="warning">{this.state.invalid_password}</p>
          </Form.Group>

          <Button type="submit" variant="primary">
            LOGIN
          </Button>
          <Form.Group>
            <Link to="/signin">
              <Button variant="primary" className="signin">
                NEW USER? SIGN IN
              </Button>
            </Link>
          </Form.Group>
        </Form>
      </div>
    );
  }
}

export default withRouter(Login);
