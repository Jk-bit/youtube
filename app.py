from flask import Flask, send_from_directory
#from flask-restful import Api, Resource, reqparse
from flask_cors import CORS #comment this on deployment
from Api_handlers.trial import TrialApiHandler
#from flask_mongoengine import MongoEngine
from flask_pymongo import PyMongo
from bson.json_util import dumps
#from bson.object_id import ObjectId
from flask import jsonify, request, make_response
from werkzeug.security import generate_password_hash, check_password_hash
from apiclient.discovery import build



#The Flask app is built here static url path denoting the  starting path of the webapp
#whilw static_folder denotes the directory where the frontend pages are present
app = Flask(__name__, static_url_path="", static_folder="frontend/build")
app.secret_key = "secret"
#APi method returns the api which can handlethe APIs
#api = Api(app)
CORS(app)

api_key = "AIzaSyBP0yGictY6IY_3D0U1a67TBQRC7hgVj64"
youtube_api = build("youtube", "v3", developerKey=api_key)

youtubedb = PyMongo(app, uri="mongodb://localhost:27017/youtubedb")
youtubedb = youtubedb.db
#db = MongoEngine()
#db.init_app(app)

@app.route("/trending", methods=["GET"])
def trending() :
    trending_results = youtube_api.videos().list(
                part="snippet",
                chart="mostPopular",
                maxResults=30
            )
    
    client_resp = {"items" : []}
    try :
        response = trending_results.execute()
        for item in response["items"] :
            client_resp["items"].append([item["id"], 
                item["snippet"]["channelTitle"],
                item["snippet"]["description"],
                (item["snippet"]["thumbnails"]["medium"]["url"], item["snippet"]["thumbnails"]["medium"]["width"], item["snippet"]["thumbnails"]["medium"]["height"]),
                item["snippet"]["title"]])
    except :
        pass
    client_response = make_response(jsonify({"items" : client_resp["items"]}), 200)
    client_response.headers["Content-Type"] = "application/json"
    return client_response


@app.route("/subscriptions", methods=["GET"])
def subscriptions() :
    emailaddress = request.args.get("emailaddress")
    query = list(youtubedb.users.find({"emailaddress" : emailaddress}))
    print(query[0]["searchList"])
    response = make_response(jsonify({"subscriptions" : query[0]["searchList"]}), 200)
    return response

@app.route("/deleteSubscription", methods=["GET"])    
def deleteSubscriptions() :
    emailaddress = request.args.get("emailaddress")
    delSubs = request.args.get("subscriptions")
    query = list(youtubedb.users.find({"emailaddress" : emailaddress}))
    searchList = query[0]["searchList"]
    searchList.remove(delSubs)
    youtubedb.users.update({"emailaddress" : emailaddress}, {"$set": {"searchList" : searchList}})
    response = make_response(jsonify({}), 200)
    return response

@app.route("/email", methods=["GET"])
def DeleteUser() :
    emailAddress = request.args.get("email")
    youtubedb.users.remove({"emailaddress" : emailAddress})
    response = make_response(jsonify({}), 200)
    return response

@app.route("/searchList", methods=["GET"])
def searchList() :
    emailAddress = request.args.get("emailaddress")
    print(emailAddress)
    client_response = {"items" : []}
    query = list(youtubedb.users.find({"emailaddress" : emailAddress}))
    print(query)
    
    if(len(query[0]["searchList"]) == 0) :
            print("length zero")
            response = make_response(jsonify({"items" : []}), 200)
            return response
    else :
            for search in query[0]["searchList"] :
                search_results = youtube_api.search().list(
                    part="snippet",
                    q = search,
                    type="video",
                    maxResults=30
                    )
                response = search_results.execute()
                for item in response["items"] :
                    client_response["items"].append([item["id"]["videoId"], 
                            item["snippet"]["channelTitle"],
                            item["snippet"]["description"],
                            (item["snippet"]["thumbnails"]["medium"]["url"], item["snippet"]["thumbnails"]["medium"]["width"], item["snippet"]["thumbnails"]["medium"]["height"]),
                            item["snippet"]["title"]])
            client_response = make_response(jsonify({"items" : client_response["items"]}), 200)
            client_response.headers["Content-Type"] = "application/json"
            return client_response
    #except :
     #   print("Gndla")
     #   response = make_response(jsonify({"items" : []}), 200)
     #   return response



@app.route("/search", methods=["GET"])
def search() :
    search = request.args.get("search")
    emailaddress = request.args.get("emailaddress")
    print("emaill address", emailaddress)
    query = list(youtubedb.users.find({"emailaddress" : emailaddress}))
    print(query[0])
    try :
        searchList = query[0]["searchList"]
    except:
        searchList = []
    if(search not in searchList) :
        searchList.append(search)
    youtubedb.users.update({"emailaddress" : emailaddress}, {"$set" : {"searchList" : searchList}})
    search_results = youtube_api.search().list(
                    part = "snippet",
                    q=search,
                    type="video",
                    maxResults=30
                )
    client_response = {"items" : []}
    response = search_results.execute()
    for item in response["items"] :
        client_response["items"].append([item["id"]["videoId"], 
            item["snippet"]["channelTitle"],
            item["snippet"]["description"],
            (item["snippet"]["thumbnails"]["medium"]["url"], item["snippet"]["thumbnails"]["medium"]["width"], item["snippet"]["thumbnails"]["medium"]["height"]),
            item["snippet"]["title"]])
    client_response = make_response(jsonify({"items" : client_response["items"]}), 200)
    client_response.headers["Content-Type"] = "application/json"
    return client_response

@app.route("/login", methods=["POST"])
def login():
    print("login")
    print(request.json)
    _json = request.json
    _emailaddress = _json["emailaddress"]
    _password = _json["password"]
    query = list(youtubedb.users.find({"emailaddress" : _emailaddress}))
    if(len(query) == 0) :
        response = {
            "status" : 200,
            "message" : "No such user",
            "no_username" : True,
            "invalid_password" : False,
              "Access-Control-Allow-Origin": "*"
        }
        return response
    if(query[0]["password"] != _password) :
        response = {
            "status" : 200,
            "message" : "No such user",
            "no_username" : False,
            "invalid_password" : True,
              "Access-Control-Allow-Origin": "*"
        }
        return response

    response = {
        "status" : 200,
        "message" : "Success",
        "no_username" : False,
        "invalid_password" : False,
        "error" : False,  
        "Access-Control-Allow-Origin": "*"

    }
    return response


@app.route("/signin", methods=["POST"])
def signin() :
    print(request)
    #_json = request.json()
    print(request.json)
    _json = request.json
    _firstName = _json["firstname"]
    _lastName = _json["lastname"]
    _emailaddress = _json["emailaddress"]
    _password = _json["password"]

    query = list(youtubedb.users.find({"emailaddress" : _emailaddress}))
    if(len(query) == 0) :
            response = make_response(
                jsonify(
                    {"error" : False}
                ), 200)
            response.headers["Content-Type"] = "application/json"
            youtubedb.users.insert({"firstname" : _firstName, "lastname" : _lastName, "emailaddress" : _emailaddress, "password" : _password, "searchList" : []})
            return response
    response = make_response(jsonify({"error" : True}),200)
    response.headers["Content-Type"] = "application/json"
    return response
#@app.route decorator denotes that whenever the the first arg paath is called
#this function should be called
@app.route("/dummy")
def dummy_func() :
    response = {"status" : 200, 
            "message" : "Success", 
            "Access-Control-Allow-Origin": "*"
        }
    return response
#api.add_resorce 
#first arg file which will handle the APIs
#second arg : the URL which will revoke the API
#api.add_resource(trial, '/trial')

if __name__ == "__main__":
    app.run(debug=True)
